package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"regexp"
	"sort"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	nodePool         = flag.String("pool", "", "Node pool")
	namespaceExclude = flag.String("exclude-ns", "", "Exclude namespaces by RegExp")
)

func main() {
	err := realMain()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func realMain() error {
	flag.Parse()

	namespaceExcludeRE, err := regexp.Compile(*namespaceExclude)
	if err != nil {
		return err
	}

	cli, err := NewClientSet()
	if err != nil {
		return err
	}

	listOpts := metav1.ListOptions{}
	if *nodePool != "" {
		listOpts.LabelSelector = fmt.Sprintf("cloud.google.com/gke-nodepool=%s", *nodePool)
	}
	nodeList, err := cli.CoreV1().Nodes().List(context.Background(), listOpts)
	if err != nil {
		return err
	}

	for _, node := range nodeList.Items {
		if nodePool, ok := node.Labels["cloud.google.com/gke-nodepool"]; ok {
			fmt.Printf("%s/%s\n", nodePool, node.Name)
		} else {
			fmt.Println(node.Name)
		}
		podList, err := cli.CoreV1().Pods("").List(context.Background(), metav1.ListOptions{
			FieldSelector: fmt.Sprintf("spec.nodeName=%s", node.Name),
		})
		if err != nil {
			fmt.Println(err)
			continue
		}
		var pods []string
		for _, pod := range podList.Items {
			if *namespaceExclude != "" && namespaceExcludeRE.MatchString(pod.Namespace) {
				continue
			}
			if pod.Status.Phase != corev1.PodRunning {
				continue
			}
			podName := fmt.Sprintf("%s/%s", pod.Namespace, pod.Name)
			pods = append(pods, podName)
		}
		sort.Strings(pods)
		for _, pod := range pods {
			fmt.Println("  * ", pod)
		}
	}

	return nil
}
