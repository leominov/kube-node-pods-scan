# kube-node-pods-scan

Листинг нод с подами на них.

## Применение

```shell
Usage of ./kube-node-pods-scan:
  -exclude-ns string
    	Exclude namespaces by RegExp
  -pool string
    	Node pool
```

## Пример отчета

```shell
gke-kube-pltf-prod-app-53b04c08-2g72 (app)
  *  default/nginx-ingress-controller-56895b5685-v55p2
  *  kube-system/kube-proxy-gke-kube-pltf-prod-app-53b04c08-2g72
  *  logging/fluentbit-kw2f9
  *  monitoring/monitoring-prometheus-node-exporter-gmd6v
gke-kube-pltf-prod-app-53b04c08-ybjr (app)
  *  default/nginx-ingress-controller-56895b5685-9d88j
  *  default/nginx-ingress-qrator-controller-754b4676d8-2zlmz
  *  kube-system/kube-proxy-gke-kube-pltf-prod-app-53b04c08-ybjr
  *  logging/fluentbit-gqrgn
  *  monitoring/monitoring-prometheus-node-exporter-wflpx
gke-kube-pltf-prod-app-6dfb5c72-fa7a (app)
  *  default/nginx-ingress-controller-56895b5685-pgkkm
  *  kube-system/coredns-coredns-6584b474bd-cqj57
  *  kube-system/kube-dns-59844ff879-gkt9j
  *  kube-system/kube-proxy-gke-kube-pltf-prod-app-6dfb5c72-fa7a
  *  kube-system/l7-default-backend-5d7d4cfccb-97cmx
  *  kube-system/metrics-server-v0.3.6-6fb8f85ccb-f6k9v
  *  logging/fluentbit-b7pzj
  *  monitoring/monitoring-kube-state-metrics-85f6d89b4b-nw86t
  *  monitoring/monitoring-prometheus-node-exporter-jz4xk
  *  rabbitmq/prod-rabbitmq-1
gke-kube-pltf-prod-app-6dfb5c72-oofm (app)
  *  cert-manager/cert-manager-7c99c65988-xsfz2
  *  cert-manager/cert-manager-cainjector-7fff9b5-7jmw4
  *  cert-manager/cert-manager-webhook-65d4c5cf44-4nvd6
  *  default/nginx-ingress-controller-56895b5685-gbh89
  *  kube-system/external-dns-79c77f68ff-5kj42
  *  kube-system/kube-proxy-gke-kube-pltf-prod-app-6dfb5c72-oofm
  *  logging/fluentbit-nnktj
  *  monitoring/monitoring-prometheus-node-exporter-7vgnc
gke-kube-pltf-prod-app-8bdfa8d8-2dly (app)
  *  blog/blog-wordpress-75896456c4-4lndw
  *  blog/blog-wordpress-75896456c4-7cjqc
  *  blog/blog-wordpress-mariadb-master-0
  *  kube-system/kube-dns-59844ff879-4m5mw
  *  kube-system/kube-proxy-gke-kube-pltf-prod-app-8bdfa8d8-2dly
  *  logging/fluentbit-jgrv5
  *  monitoring/monitoring-prometheus-node-exporter-6nlck
gke-kube-pltf-prod-app-8bdfa8d8-96hq (app)
  *  kube-system/kube-proxy-gke-kube-pltf-prod-app-8bdfa8d8-96hq
  *  logging/fluentbit-xmwmx
  *  monitoring/monitoring-prometheus-node-exporter-dmz7q
gke-kube-pltf-prod-app-8bdfa8d8-jy5h (app)
  *  default/nginx-ingress-controller-56895b5685-v8v87
  *  kube-system/kube-proxy-gke-kube-pltf-prod-app-8bdfa8d8-jy5h
  *  logging/fluentbit-ftwvr
  *  monitoring/monitoring-prometheus-node-exporter-lmj2q
```
